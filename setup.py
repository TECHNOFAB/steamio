import re
from setuptools import setup, find_packages

with open('requirements.txt') as f:
    requirements = f.read().splitlines() or []

with open('README.md') as f:
    readme = f.read() or ''

version = ''
with open('steamio/__init__.py') as f:
    version = re.search(r'^__version__\s*=\s*[\'"]([^\'"]*)[\'"]', f.read(), re.MULTILINE).group(1)

if not version:
    raise RuntimeError('Version is not set!')

if version.endswith(('a', 'b', 'rc')):
    try:
        import subprocess

        p = subprocess.Popen(['git', 'rev-list', '--count', 'HEAD'],
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()
        if out:
            version += out.decode('utf-8').strip()
    except Exception:
        pass

extra_requirements = {
    'docs': [
        'sphinx',
        'sphinxcontrib_trio'
    ]
}

setup(
    name='steamio',
    version=version,
    description='Async Chat/Bot wrapper for Steam Chat made in Python',
    author='Technofab',
    author_email='steamio.git@technofab.de',
    url='https://gitlab.com/TECHNOFAB/steamio',
    license='MIT',
    packages=['steamio', 'steamio.ext.commands', 'steamio.core', 'steamio.core.protobufs'],
    long_description=readme,
    long_description_content_type="text/markdown",
    install_requires=requirements,
    extras_require=extra_requirements,
    python_requires='>=3.6',
)
