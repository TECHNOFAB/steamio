# -*- coding: utf-8 -*-
import steamio

client = steamio.Client(username="", password="")

@client.event
async def on_ready():
    print(f"Logged in as {client.user.name}")
    print(f"I have {len(client.licenses)} games/apps!")
    print(f"I have {len(client.friends)} friends")

@client.event
async def on_message(user, message):
    await user.reply(message)  # sends back the same message

client.run()  # raises the same Exceptions as start(), see below
#  OR for already running EventLoops
async def main():
    try:
        await client.start()  # add two_factor='<Token>' params if you have 2FA enabled
    # see docs for exceptions that can happen here
    finally:
        await client.close()
