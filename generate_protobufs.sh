#!/bin/bash

output_path="../steamio/core/protobufs"

get_protobufs() {
  echo "Getting Protobuf List..."
  wget -nv --show-progress https://raw.githubusercontent.com/ValvePython/steam/master/protobuf_list.txt || exit 0
  echo "Getting Protobufs..."
  wget -nv --show-progress -N -P ./protobufs/ -i protobuf_list.txt || exit 0
}

rename_protobufs() {
  mv protobufs/friends.proto protobufs/steammessages_webui_friends.steamclient.proto
	sed -i 's/CCommunity_ClanAnnouncementInfo/xCCommunity_ClanAnnouncementInfo/' protobufs/steammessages_webui_friends.steamclient.proto
	sed -i 's/CMsgClientSecret/xCMsgClientSecret/' protobufs/steammessages_webui_friends.steamclient.proto
	sed -i '1s/^/option py_generic_services = true\;\n/' protobufs/steammessages_webui_friends.steamclient.proto
	rename -v '.proto' '.proto.notouch' protobufs/{steammessages_physicalgoods,gc,test_messages}.proto
	rename -v '.steamclient' '' protobufs/*.proto
	sed -i '1s/^/syntax = "proto2"\;\n/' protobufs/*.proto
	sed -i 's/cc_generic_services/py_generic_services/' protobufs/*.proto
	sed -i 's/\.steamclient\.proto/.proto/' protobufs/*.proto
	rename -v '.notouch' '' protobufs/*.proto.notouch
}

compile_protobufs() {
  echo "Compiling Protobufs..."
  for filepath in ./protobufs/*.proto; do
		protoc --python_out $output_path --proto_path=./protobufs "$filepath"
	done
}

mkdir .prototemp
cd .prototemp || exit 0

get_protobufs
rename_protobufs
compile_protobufs

echo "Done. Removing temp directory..."

cd ..
rm -rf .prototemp