# -*- coding: utf-8 -*-
import asyncio
import logging
import time
from binascii import hexlify
from datetime import datetime
from enum import IntEnum
from typing import Union

from .core.enums import EFriendRelationship, EMsg, EChatEntryType
from .core.msg import MsgProto
from .core.steamid import SteamID
from .game import UserGame

_ATTRS = ['avatar_hash', 'broadcast_id', 'clan_data', 'clan_rank', 'clan_tag',
          'friendid', 'game_data_blob', 'game_lobby_id', 'game_name',
          'game_played_app_id', 'game_server_ip', 'game_server_port', 'gameid',
          'last_logoff', 'last_logon', 'last_seen_online', 'online_session_instances',
          'persona_set_by_user', 'persona_state', 'persona_state_flags', 'player_name',
          'query_port', 'rich_presence', 'steamid_source', 'watching_broadcast_accountid',
          'watching_broadcast_appid', 'watching_broadcast_title',
          'watching_broadcast_viewers']


class User:
    relationship = EFriendRelationship.NONE
    _state = None
    _state_event = asyncio.Event()

    def __init__(self, client, steamid: Union[SteamID, int]):
        self._client = client
        self._log = logging.getLogger(__name__)
        self.steamid = SteamID(steamid) if type(steamid) == int else steamid

    @property
    def ready(self):
        return self._state_event.is_set() and self.state is not None

    def _get(self, attr):
        try:
            return getattr(self._state, attr)
        except AttributeError:
            return None

    def parse(self):
        self._state_event.set()

    async def update(self):
        self._state_event.clear()
        await self._client.fetch_state([self.steamid])
        await self._state_event.wait()

    async def send_message(self, message):
        if self._client.chat_mode == 2:
            await self._client.send_um("FriendMessages.SendMessage#1", {
                'steamid': self.steamid,
                'chat_entry_type': EChatEntryType.ChatMsg,
                'message': message,
            })
        else:
            await self._client.send(MsgProto(EMsg.ClientFriendMsg), {
                'steamid': self.steamid,
                'chat_entry_type': EChatEntryType.ChatMsg,
                'message': message.encode('utf8'),
            })

    reply = send_message

    @property
    def id(self):
        return self.steamid

    @property
    def name(self):
        return self._get('player_name')

    @property
    def last_logon(self):
        ts = self._get('last_logon')
        return datetime.utcfromtimestamp(ts) if ts else None

    @property
    def last_logoff(self):
        ts = self._get('last_logoff')
        return datetime.utcfromtimestamp(ts) if ts else None

    @property
    def last_seen_online(self):
        ts = self._get('last_seen_online')
        return datetime.utcfromtimestamp(ts) if ts else None

    @property
    def game(self):
        return UserGame(self._state) if self.ready else None

    def avatar_url(self, size=2):
        hashbytes = self._get('avatar_hash')

        if hashbytes != "\000\000\000\000\000\000\000\000\000\000\000" \
                        "\000\000\000\000\000\000\000\000\000":
            ahash = hexlify(hashbytes).decode('ascii')
        else:
            ahash = 'fef49e7fa7e1997310d705b2a6158ff8dc1cdfeb'

        sizes = {
            0: '',
            1: '_medium',
            2: '_full',
        }
        url = f"http://cdn.akamai.steamstatic.com/steamcommunity/public/images/" \
              f"avatars/{ahash[:2]}/{ahash}{sizes[size]}.jpg"

        return url

    @property
    def state(self):
        s = self._get('persona_state')
        return PersonaState(s) if s is not None else None


class PersonaState(IntEnum):
    Offline = 0
    Online = 1
    Busy = 2
    Away = 3
    Snooze = 4
    LookingToTrade = 5
    LookingToPlay = 6
    Max = 7


class FriendList:
    ready = False
    _friends = {}
    _last_update = None

    @property
    def last_updated(self):
        return self._last_update

    def set(self, friends):
        self._last_update = time.time()
        self._friends = friends

    def update(self, friends):
        self._last_update = time.time()
        self._friends.update(friends)

    def add(self, friend):
        self._last_update = time.time()
        self._friends[friend.id] = friend

    def __len__(self):
        return len(self._friends)

    def __iter__(self):
        return iter(self._friends.values())

    def __list__(self):
        return list(iter(self))

    def __getitem__(self, key):
        if isinstance(key, User):
            key = key.id
        return self._friends[key]

    def __contains__(self, user):
        if isinstance(user, User):
            user = user.id
        return user in self._friends
