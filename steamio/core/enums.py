from enum import IntEnum


class EResult(IntEnum):
    Invalid = 0
    OK = 1
    InvalidPassword = 5
    FileNotFound = 9
    ServiceUnavailable = 20
    TryAnotherCM = 48
    InvalidLoginAuthCode = 65
    AccountLoginDeniedNeedTwoFactor = 85
    TwoFactorCodeMismatch = 88


class EMsg(IntEnum):
    Invalid = 0
    Multi = 1

    ServiceMethod = 146
    ServiceMethodResponse = 147
    ServiceMethodCallFromClient = 151
    ServiceMethodSendToClient = 152

    ClientHeartBeat = 703
    ClientLogOff = 706
    ClientChangeStatus = 716
    ClientLogOnResponse = 751
    ClientPersonaState = 766
    ClientFriendsList = 767
    ClientAccountInfo = 768
    ClientGameConnectTokens = 779
    ClientLicenseList = 780
    ClientVACBanStatus = 782
    ClientCMList = 783
    ClientUpdateGuestPassesList = 798
    ClientRequestFriendData = 815
    ClientClanState = 822
    ClientSessionToken = 850

    ChannelEncryptRequest = 1303
    ChannelEncryptResponse = 1304
    ChannelEncryptResult = 1305

    ClientFriendMsgIncoming = 5427
    ClientIsLimitedAccount = 5430
    ClientToGC = 5452
    ClientFromGC = 5453
    ClientEmailAddrInfo = 5456
    ClientNewLoginKeyAccepted = 5464
    ClientNewLoginKey = 5463
    ClientRequestedClientStats = 5480
    ClientServersAvailable = 5501
    ClientLogon = 5514
    ClientMarketingMessageUpdate2 = 5510
    ClientWalletInfoUpdate = 5528
    ClientUpdateMachineAuth = 5537
    ClientUpdateMachineAuthResponse = 5538
    ClientFriendsGroupsList = 5553
    ClientLogonGameServer = 5559
    ClientPlayerNicknameList = 5587
    ClientGetNumberOfCurrentPlayersDP = 5592
    ClientGetNumberOfCurrentPlayersDPResponse = 5593
    ClientServiceMethod = 5594
    ClientServiceMethodResponse = 5595
    ClientCurrentUIMode = 5597

    ClientChatOfflineMessageNotification = 7523

    ClientConcurrentSessionsBase = 9600


class EUniverse(IntEnum):
    Invalid = 0
    Public = 1
    Beta = 2
    Internal = 3
    Dev = 4
    Max = 6


class EOSType(IntEnum):
    Unknown = -1
    Web = -700
    IOSUnknown = -600
    IOS1 = -599
    IOS2 = -598
    IOS3 = -597
    IOS4 = -596
    IOS5 = -595
    IOS6 = -594
    IOS6_1 = -593
    IOS7 = -592
    IOS7_1 = -591
    IOS8 = -590
    IOS8_1 = -589
    IOS8_2 = -588
    IOS8_3 = -587
    IOS8_4 = -586
    IOS9 = -585
    IOS9_1 = -584
    IOS9_2 = -583
    IOS9_3 = -582
    IOS10 = -581
    IOS10_1 = -580
    IOS10_2 = -579
    IOS10_3 = -578
    IOS11 = -577
    IOS11_1 = -576
    IOS11_2 = -575
    IOS11_3 = -574
    IOS11_4 = -573
    IOS12 = -572
    IOS12_1 = -571
    AndroidUnknown = -500
    Android6 = -499
    Android7 = -498
    Android8 = -497
    Android9 = -496
    UMQ = -400
    PS3 = -300
    MacOSUnknown = -102
    MacOS104 = -101
    MacOS105 = -100
    MacOS1058 = -99
    MacOS106 = -95
    MacOS1063 = -94
    MacOS1064_slgu = -93
    MacOS1067 = -92
    MacOS107 = -90
    MacOS108 = -89
    MacOS109 = -88
    MacOS1010 = -87
    MacOS1011 = -86
    MacOS1012 = -85
    Macos1013 = -84
    Macos1014 = -83
    Macos1015 = -82
    MacOSMax = -1
    LinuxUnknown = -203
    Linux22 = -202
    Linux24 = -201
    Linux26 = -200
    Linux32 = -199
    Linux35 = -198
    Linux36 = -197
    Linux310 = -196
    Linux316 = -195
    Linux318 = -194
    Linux3x = -193
    Linux4x = -192
    Linux41 = -191
    Linux44 = -190
    Linux49 = -189
    Linux414 = -188
    Linux419 = -187
    Linux5x = -186
    LinuxMax = -101
    WinUnknown = 0
    Win311 = 1
    Win95 = 2
    Win98 = 3
    WinME = 4
    WinNT = 5
    Win2000 = 6
    WinXP = 7
    Win2003 = 8
    WinVista = 9
    Windows7 = 10
    Win2008 = 11
    Win2012 = 12
    Windows8 = 13
    Windows81 = 14
    Win2012R2 = 15
    Windows10 = 16
    Win2016 = 17
    WinMAX = 18
    Max = 26


class EChatEntryType(IntEnum):
    Invalid = 0
    ChatMsg = 1             #: Normal text message from another user
    Typing = 2              #: Another user is typing (not used in multi-user chat)
    InviteGame = 3          #: Invite from other user into that users current game
    LeftConversation = 6    #: user has left the conversation (closed chat window)
    Entered = 7             #: user has entered the conversation (group chat etc.)
    WasKicked = 8           #: user was kicked
    WasBanned = 9           #: user was banned
    Disconnected = 10       #: user disconnected
    HistoricalChat = 11     #: a chat message from user's chat history or offline message
    LinkBlocked = 14        #: a link was removed by the chat filter


class EUserType(IntEnum):
    Invalid = 0
    Individual = 1  #: single user account
    Multiseat = 2  #: multiseat (e.g. cybercafe) account
    GameServer = 3  #: game server account
    AnonGameServer = 4  #: anonymous game server account
    Pending = 5  #: pending
    ContentServer = 6  #: content server
    Clan = 7
    Chat = 8
    ConsoleUser = 9  #: Fake SteamID for local PSN account on PS3/Live account on 360,etc.
    AnonUser = 10
    Max = 11


class EUserTypeChar(IntEnum):
    I = EUserType.Invalid  # noqa: E741
    U = EUserType.Individual
    M = EUserType.Multiseat
    G = EUserType.GameServer
    A = EUserType.AnonGameServer
    P = EUserType.Pending
    C = EUserType.ContentServer
    g = EUserType.Clan
    T = EUserType.Chat
    L = EUserType.Chat  # lobby chat, 'c' for clan chat
    c = EUserType.Chat  # clan chat
    a = EUserType.AnonUser


class EInstanceFlag(IntEnum):
    MMSLobby = 0x20000
    Lobby = 0x40000
    Clan = 0x80000


class EFriendRelationship(IntEnum):
    NONE = 0
    Blocked = 1
    RequestRecipient = 2
    Friend = 3
    RequestInitiator = 4
    Ignored = 5
    IgnoredFriend = 6
    SuggestedFriend = 7
    Max = 8


class EOwnershipType(IntEnum):
    """
    See https://partner.steamgames.com/doc/api/steam_api -> EAppOwnershipFlags
    """
    Unknown = 0x0000
    OwnsLicense = 0x0001
    FreeLicense = 0x0002
    RegionRestricted = 0x0004
    LowViolence = 0x0008
    InvalidPlatform = 0x0010
    SharedLicense = 0x0020
    FreeWeekend = 0x0040
    RetailLicense = 0x0080
    LicenseLocked = 0x0100
    LicensePending = 0x0200
    LicenseExpired = 0x0400
    LicensePermanent = 0x0800
    LicenseRecurring = 0x1000
    LicenseCanceled = 0x2000
    AutoGrant = 0x4000
    PendingGift = 0x8000
    RentalNotActivated = 0x10000
    Rental = 0x20000
    SiteLicense = 0x40000


class EAppType(IntEnum):
    """
    See https://partner.steamgames.com/doc/api/steam_api -> EAppType
    """
    Invalid = 0x000
    Game = 0x001
    Application = 0x002
    Tool = 0x004
    Demo = 0x008
    DLC = 0x020
    Guide = 0x040
    Driver = 0x080
    Config = 0x100
    Hardware = 0x200
    Franchise = 0x400
    Video = 0x800
    Plugin = 0x1000
    Music = 0x2000
    Series = 0x4000
