# -*- coding: utf-8 -*-
import struct

from steamio.core.enums import EMsg
from steamio.core.protobufs import steammessages_base_pb2
from steamio.core.structs import get_struct
from steamio.core.utils import clear_proto_bit, set_proto_bit, get_um, get_cmsg


class Msg:
    proto = False
    payload = None
    body = None

    def __init__(self, msg, data=None, extended=False, parse=True):
        self.extended = extended
        self.header = MsgHeader(data) if not extended else ExtendedMsgHdr(data)
        self.msg = msg

        if data:
            self.payload = data[self.header._size:]
        if parse:
            self.parse()

    def parse(self):
        if self.body is None:
            deserializer = get_struct(self.msg)

            if deserializer:
                self.body = deserializer(self.payload)
                self.payload = None
            else:
                self.body = None
                raise RuntimeError(f'No deserializer found! EMsg: {repr(self.msg)}')

    def serialize(self):
        return self.header.serialize() + self.body.serialize()

    @property
    def msg(self):
        return self.header.msg

    @msg.setter
    def msg(self, value):
        self.header.msg = EMsg(value)

    @property
    def steamID(self):
        return (self.header.steamID
                if isinstance(self.header, ExtendedMsgHdr)
                else None
                )

    @steamID.setter
    def steamID(self, value):
        if isinstance(self.header, ExtendedMsgHdr):
            self.header.steamID = value

    @property
    def sessionID(self):
        return (self.header.sessionID
                if isinstance(self.header, ExtendedMsgHdr)
                else None
                )

    @sessionID.setter
    def sessionID(self, value):
        if isinstance(self.header, ExtendedMsgHdr):
            self.header.sessionID = value


class MsgProto:
    proto = True
    payload = None
    body = None

    def __init__(self, msg, data=None, parse=True):
        self._header = MsgHdrProtoBuf(data)
        self.header = self._header.proto
        self.msg = msg

        if data:
            self.payload = data[self._header._fullsize:]

        if parse:
            self.parse()

    def parse(self):
        if self.body is None:
            if self.msg in (EMsg.ServiceMethod, EMsg.ServiceMethodResponse,
                            EMsg.ServiceMethodSendToClient):
                is_resp = False if self.msg == EMsg.ServiceMethod else True
                proto = get_um(self.header.target_job_name, response=is_resp)
            else:
                proto = get_cmsg(self.msg)

            if proto:
                self.body = proto()
                if self.payload:
                    self.body.ParseFromString(self.payload)
                    self.payload = None
            else:
                self.body = '!!! Failed to resolve message !!!'

    def serialize(self):
        return self._header.serialize() + self.body.SerializeToString()

    @property
    def msg(self):
        return self._header.msg

    @msg.setter
    def msg(self, value):
        self._header.msg = EMsg(value)

    @property
    def steamID(self):
        return self.header.steamid

    @steamID.setter
    def steamID(self, value):
        self.header.steamid = value

    @property
    def sessionID(self):
        return self.header.client_sessionid

    @sessionID.setter
    def sessionID(self, value):
        self.header.client_sessionid = value


class MsgHeader:
    _size = struct.calcsize("<Iqq")
    msg = EMsg.Invalid
    targetJobID = -1
    sourceJobID = -1

    def __init__(self, data=None):
        if data:
            self.load(data)

    def serialize(self):
        return struct.pack("<Iqq", self.msg, self.targetJobID, self.sourceJobID)

    def load(self, data):
        (msg, self.targetJobID, self.sourceJobID) = struct.unpack_from("<Iqq", data)
        self.msg = EMsg(msg)


class MsgHdrProtoBuf:
    _size = _fullsize = struct.calcsize("<II")
    msg = EMsg.Invalid

    def __init__(self, data=None):
        self.proto = steammessages_base_pb2.CMsgProtoBufHeader()

        if data:
            self.load(data)

    def serialize(self):
        proto_data = self.proto.SerializeToString()
        return struct.pack("<II", set_proto_bit(self.msg), len(proto_data)) + proto_data

    def load(self, data):
        msg, proto_length = struct.unpack_from("<II", data)

        self.msg = EMsg(clear_proto_bit(msg))
        size = self._size
        self._fullsize = size + proto_length
        self.proto.ParseFromString(data[size:self._fullsize])


class ExtendedMsgHdr:
    _size = struct.calcsize("<IBHqqBqi")
    msg = EMsg.Invalid
    headerSize = 36
    headerVersion = 2
    targetJobID = -1
    sourceJobID = -1
    headerCanary = 239
    steamID = -1
    sessionID = -1

    def __init__(self, data=None):
        if data:
            self.load(data)

    def serialize(self):
        return struct.pack("<IBHqqBqi", self.msg, self.headerSize, self.headerVersion,
                           self.targetJobID,
                           self.sourceJobID, self.headerCanary, self.steamID,
                           self.sessionID)

    def load(self, data):
        (msg, self.headerSize, self.headerVersion, self.targetJobID, self.sourceJobID,
         self.headerCanary, self.steamID,
         self.sessionID,) = struct.unpack_from("<IBHqqBqi", data)

        self.msg = EMsg(msg)

        if self.headerSize != 36 or self.headerVersion != 2:
            raise RuntimeError("Failed to parse header")
