# -*- coding: utf-8 -*-
import struct
import six

from .binary import StructReader
from .enums import EMsg, EUniverse, EResult

_emsg_map = {}


def get_struct(emsg):
    return _emsg_map.get(emsg, None)


class StructMessageMeta(type):
    def __new__(mcs, name, bases, classdict):
        cls = type.__new__(mcs, name, bases, classdict)

        if name != 'StructMessage':
            try:
                _emsg_map[EMsg[name]] = cls
            except KeyError:
                pass
        return cls


@six.add_metaclass(StructMessageMeta)
class StructMessage:
    def __init__(self, data=None):
        if data:
            self.load(data)

    def serialize(self):
        raise NotImplementedError

    def load(self, data):
        raise NotImplementedError


class ChannelEncryptRequest(StructMessage):
    protocolVersion = 1
    universe = EUniverse.Invalid
    challenge = b''

    def serialize(self):
        return struct.pack("<II", self.protocolVersion, self.universe) + self.challenge

    def load(self, data):
        self.protocolVersion, universe, = struct.unpack_from("<II", data)

        self.universe = EUniverse(universe)

        if len(data) > 8:
            self.challenge = data[8:]


class ChannelEncryptResponse(StructMessage):
    protocolVersion = 1
    keySize = 128
    key = ''
    crc = 0

    def serialize(self):
        return struct.pack("<II128sII", self.protocolVersion, self.keySize, self.key,
                           self.crc, 0)

    def load(self, data):
        self.protocolVersion, self.keySize, self.key, self.crc, _, = struct.unpack_from(
            "<II128sII", data)


class ChannelEncryptResult(StructMessage):
    eresult = EResult.Invalid

    def serialize(self):
        return struct.pack("<I", self.eresult)

    def load(self, data):
        result, = struct.unpack_from("<I", data)
        self.eresult = EResult(result)


class ClientVACBanStatus(StructMessage):
    class VACBanRange(object):
        start = 0
        end = 0

    @property
    def numBans(self):
        return len(self.ranges)

    def __init__(self, data):
        self.ranges = list()
        StructMessage.__init__(self, data)

    def load(self, data):
        buf = StructReader(data)
        bans, = buf.unpack("<I")

        for _ in range(bans):
            m = self.VACBanRange()
            self.ranges.append(m)

            m.start, m.end, _ = buf.unpack("<III")

            if m.start > m.end:
                m.start, m.end = m.end, m.start


class ClientMarketingMessageUpdate2(StructMessage):
    class MarketingMessage(object):
        id = 0
        url = ''
        flags = 0

    time = 0

    @property
    def count(self):
        return len(self.messages)

    def __init__(self, data):
        self.messages = list()
        StructMessage.__init__(self, data)

    def load(self, data):
        buf = StructReader(data)
        self.time, count = buf.unpack("<II")

        for _ in range(count):
            m = self.MarketingMessage()
            self.messages.append(m)

            length, m.id = buf.unpack("<IQ")
            m.url = buf.read_cstring().decode('utf-8')
            m.flags = buf.unpack("<I")


class ClientUpdateGuestPassesList(StructMessage):
    eresult = EResult.Invalid
    countGuestPassesToGive = 0
    countGuestPassesToRedeem = 0

    def load(self, data):
        (eresult, self.countGuestPassesToGive,
         self.countGuestPassesToRedeem,) = struct.unpack_from("<III", data)

        self.eresult = EResult(eresult)
