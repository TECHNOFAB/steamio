# -*- coding: utf-8 -*-
import asyncio
import binascii
import logging
import struct
import time
from asyncio import Event
from collections import defaultdict
from gzip import GzipFile
from io import BytesIO
from itertools import cycle, count
from random import shuffle
from typing import Union

import aiohttp as aiohttp

from .crypto import symmetric_decrypt_HMAC, symmetric_decrypt, \
    symmetric_encrypt_HMAC, symmetric_encrypt, generate_session_key, sha1_hash
from .enums import EResult, EMsg, EUniverse, EFriendRelationship, EUserType, \
    EChatEntryType
from .msg import Msg, MsgProto
from .steamid import SteamID
from .utils import FMT_SIZE, MAGIC, FMT, clear_proto_bit, is_proto, \
    proto_fill_from_dict, ip_to_int, store_sentry, get_sentry, ip_from_int, get_um
from steamio.errors import InvalidPassword, TwoFactorMismatch, TwoFactorNeeded
from ..clan import Clan
from ..license import License
from ..user import User


class Connection:
    connected = False
    reconnect = True
    writer = None
    reader = None
    username = None
    password = None
    auth_code = None
    two_factor_code = None
    channel_key = None
    channel_hmac = None
    logged_on = False
    cell_id = None
    steam_id = None
    session_id = None
    exception = None
    login_key = None
    chat_mode = 2
    current_jobid = 0
    disconnect_event = Event()
    channel_secured = Event()
    _encrypt_event = Event()
    _heartbeat_task = None
    _encrypt_event_msg = None
    _current_server = None
    _i = count(0)
    _buffer = b''

    def __init__(self, dispatcher, client, verbose_logging: bool):
        self._log = logging.getLogger(__name__)
        self._client = client
        self.dispatcher = dispatcher
        self.servers = Servers()
        self.verbose_logging = verbose_logging

    @property
    def local_address(self):
        """
        Local IP Address
        """
        try:
            return self.writer.get_extra_info('sockname')[0]
        except AttributeError:
            return None

    async def connect(self):
        """
        Gathers servers, then tries to connect with one after another until success
        """
        self.logged_on = False
        self.disconnect_event.clear()
        while len(self.servers) == 0:
            await self.servers.from_webapi()

        for i, server in enumerate(cycle(self.servers), start=next(self._i)):
            self._log.debug(f'Connecting to {server[0]}:{server[1]}...')
            if await self._connect_to(server[0], server[1]):
                break

        if not self.channel_secured.is_set():
            try:
                self._log.debug('Waiting until channel is secured...')
                await asyncio.wait_for(self.channel_secured.wait(), timeout=30)
            except asyncio.TimeoutError:
                self._log.debug('Timed out waiting for channel securing, disconnecting..')
                await self.disconnect()
                return False

        await self.login()

    async def disconnect(self):
        """
        Disconnects from the current server or returns if not connected
        """
        if not self.connected:
            self._log.debug("Not connected, ignoring disconnect.")
            return

        self._log.debug("Disconnecting...")

        self.connected = False
        self._reader_task.cancel()
        if self._heartbeat_task and not self._heartbeat_task.done():
            self._heartbeat_task.cancel()
        self.writer.close()
        await self.writer.wait_closed()
        self.disconnect_event.set()

    async def login(self):
        message = MsgProto(EMsg.ClientLogon)
        message.body.protocol_version = 65580
        message.body.client_package_version = 1561159470

        if self.username is None:
            self._log.debug("Logging in as anonymous User...")
            message.header.steamid = SteamID(type='AnonUser', universe='Public')
        else:
            self._log.debug(f"Logging in as {self.username}...")
            message.header.steamid = SteamID(type='Individual', universe='Public')
            message.body.client_language = "english"
            message.body.should_remember_password = True
            message.body.supports_rate_limit_response = True
            message.body.chat_mode = self.chat_mode

            message.body.obfuscated_private_ip.v4 = ip_to_int(
                self.local_address) ^ 0xF00DBAAD
            message.body.account_name = self.username

            if self.login_key:
                message.body.password = self.login_key
            else:
                message.body.password = self.password

            sentry = await get_sentry(self._client.sentry_path)
            if not sentry:
                message.body.eresult_sentryfile = EResult.FileNotFound
            else:
                message.body.eresult_sentryfile = EResult.OK
                message.body.sha_sentryfile = sha1_hash(sentry)

            if self.auth_code:
                message.body.auth_code = self.auth_code
            if self.two_factor_code:
                message.body.two_factor_code = self.two_factor_code

        await self.send(message)

    async def send(self, data: Union[bytes, Msg, MsgProto], params: dict = None):
        """
        Send data to the connected server

        :param data: Data to send to the server
        :type data: Union[:class:`bytes`,:class:`Msg`,:class:`MsgProto`]
        :param params: Data to fill MsgProto with
        :type params: :class:`dict`
        """
        if not self.connected:
            self._log.debug("Not connected, discarding message...")
            return

        if isinstance(data, (Msg, MsgProto)):
            if self.steam_id:
                data.steamID = self.steam_id
            if self.session_id:
                data.sessionID = self.session_id

        if isinstance(data, Msg):
            data = data.serialize()
        elif isinstance(data, MsgProto):
            if params:
                proto_fill_from_dict(data.body, params)
            data = data.serialize()

        if self.channel_key:
            if self.channel_hmac:
                data = symmetric_encrypt_HMAC(data, self.channel_key, self.channel_hmac)
            else:
                data = symmetric_encrypt(data, self.channel_key)

        packet = struct.pack(FMT, len(data), MAGIC) + data
        try:
            if self.verbose_logging:
                self._log.debug(f"Writing {data}")
            self.writer.write(packet)
            await self.writer.drain()
        except Exception as e:
            self._log.error(f'Writer error: {e}')
            await self.disconnect()

    async def send_job(self, msg, params=None):
        jobid = self.current_jobid = ((self.current_jobid + 1) % 10000) or 1
        if msg.proto:
            msg.header.jobid_source = jobid
        else:
            msg.header.sourceJobID = jobid

        await self.send(msg, params)

    async def send_um(self, method_name, params=None):
        proto = get_um(method_name)

        if proto is None:
            raise ValueError("Failed to find method named: %s" % method_name)

        message = MsgProto(EMsg.ServiceMethodCallFromClient)
        message.header.target_job_name = method_name
        message.body = proto()

        if params:
            proto_fill_from_dict(message.body, params)

        await self.send_job(message)

    async def _connect_to(self, host: str, port: int):
        """
        Tries to connect to given host under port
        :param host: the host of the server to connect to
        :type host: :class:`str`
        :param port: the port of the server to connect to
        :type port: :class:`int`
        :return: success
        :rtype: :class:`bool`
        """
        try:
            self.reader, self.writer = await asyncio.open_connection(host, port)
        except Exception as e:
            self._log.error(f'Couldn\'t connect to {host}:{port}: {e}')
            self.servers.mark_bad((host, port))
            return False
        self.connected = True
        self._current_server = (host, port)
        self._log.debug(f'Connected with {host}:{port}')
        self._reader_task = asyncio.ensure_future(self.__reader_task())
        return True

    async def __reader_task(self):
        while True:
            data = await self.reader.read(16384)

            if not data:
                self._log.debug('Reader error')
                asyncio.ensure_future(self.disconnect())
                return

            self._buffer += data
            asyncio.ensure_future(self.__read_packets())

    async def __read_packets(self):
        buf = self._buffer

        while len(buf) > FMT_SIZE:
            message_length, magic = struct.unpack_from(FMT, buf)
            if magic != MAGIC:
                self._log.debug('Wrong magic')
                return

            packet_length = FMT_SIZE + message_length
            if len(buf) < packet_length:
                self._log.debug('Packet bigger than buffer')
                return

            message = buf[FMT_SIZE:packet_length]
            buf = buf[packet_length:]

            if self.channel_key:
                if self.channel_hmac:
                    message = symmetric_decrypt_HMAC(message, self.channel_key,
                                                     self.channel_hmac)
                else:
                    message = symmetric_decrypt(message, self.channel_key)

            await self.__parse_message(message)
        self._buffer = buf

    async def __parse_message(self, msg):
        emsg_id, = struct.unpack_from("<I", msg)
        cleared_emsg = clear_proto_bit(emsg_id)
        try:
            emsg = EMsg(cleared_emsg)
        except ValueError:
            self._log.debug(f"Couldn\'t find EMsg by id: {cleared_emsg}")
            return

        if not self.connected and emsg != EMsg.ClientLogOnResponse:
            self._log.debug(f"Dropped message {msg}")
            return

        if emsg in (EMsg.ChannelEncryptResult,
                    EMsg.ChannelEncryptRequest,
                    EMsg.ChannelEncryptResponse):
            msg = Msg(emsg, msg)
        else:
            try:
                if is_proto(emsg_id):
                    msg = MsgProto(emsg, msg)
                else:
                    msg = Msg(emsg, msg, extended=True)
            except Exception as e:
                self._log.exception(e)
                return

        await self.__handle_message(emsg, msg)

        if msg.proto:
            jobid = msg.header.jobid_target
        else:
            jobid = msg.header.targetJobID

        if jobid not in (-1, 18446744073709551615):
            jobid = "job_%d" % jobid
            await self.__handle_message(jobid, msg)

        if emsg in (EMsg.ServiceMethod, EMsg.ServiceMethodResponse,
                    EMsg.ServiceMethodSendToClient):
            await self.__handle_message(msg.header.target_job_name, msg)

    async def __handle_message(self, emsg, msg):
        def handle(func):
            asyncio.ensure_future(func(msg))

        handlers = {
            EMsg.ClientCMList: self._handle_cm_list,
            EMsg.ChannelEncryptRequest: self._handle_encryption,
            EMsg.Multi: self._handle_multi,
            EMsg.ClientLogOnResponse: self._handle_logon,
            EMsg.ClientNewLoginKey: self._handle_loginkey,
            EMsg.ClientLicenseList: self._handle_licenses,
            EMsg.ClientUpdateMachineAuth: self._handle_sentry,
            EMsg.ClientFriendsList: self._handle_friendlist,
            EMsg.ClientPersonaState: self._handle_persona_state,
            EMsg.ClientFriendMsgIncoming: self._handle_old_chat_message,
            'FriendMessagesClient.IncomingMessage#1': self._handle_new_chat_message,
            EMsg.ClientIsLimitedAccount: self._handle_limited_account,
            EMsg.ClientAccountInfo: self._handle_account_info,
            EMsg.ClientEmailAddrInfo: self._handle_email_info,
            EMsg.ClientWalletInfoUpdate: self._handle_wallet_info,
            EMsg.ClientVACBanStatus: self._handle_vac_status,
            EMsg.ClientClanState: self._handle_clan_state
        }

        if emsg in handlers:
            handle(handlers[emsg])
        elif emsg == EMsg.ChannelEncryptResult:
            self._encrypt_event_msg = msg
            self._encrypt_event.set()
        else:
            if self.verbose_logging:
                self._log.debug(f"{repr(emsg)} not handled")

        self._client._update_ready_state()

    async def _handle_cm_list(self, msg):
        self._log.debug("Updating CM Servers")
        new_servers = zip(map(ip_from_int, msg.body.cm_addresses), msg.body.cm_ports)
        self.servers.clear()
        self.servers.merge(new_servers)
        self.servers.cellid = self.cell_id

    async def _handle_encryption(self, msg):
        try:
            if msg.body.protocolVersion != 1:
                raise RuntimeError("Unsupported protocol version")
            if msg.body.universe != EUniverse.Public:
                raise RuntimeError("Unsupported universe")
        except RuntimeError as e:
            self._log.exception(e)
            await self.disconnect()
            return

        self._log.debug("Securing channel")
        self.channel_secured.clear()

        resp = Msg(EMsg.ChannelEncryptResponse)
        challenge = msg.body.challenge
        key, resp.body.key = generate_session_key(challenge)
        resp.body.crc = binascii.crc32(resp.body.key) & 0xffffffff

        await self.send(resp)

        try:
            await asyncio.wait_for(self._encrypt_event.wait(), timeout=5)
        except asyncio.TimeoutError:
            self.servers.mark_bad(self._current_server)
            await self.disconnect()
            return

        eresult = self._encrypt_event_msg.body.eresult

        if eresult != EResult.OK:
            self._log.error(f'Failed to secure channel: {eresult}')
            asyncio.ensure_future(self.disconnect())
            return

        self.channel_key = key

        if challenge:
            self._log.debug("Channel secured")
            self.channel_hmac = key[:16]
        else:
            self._log.debug("Channel secured (legacy mode)")

        self.channel_secured.set()

    async def _handle_multi(self, msg):
        self._log.debug("Multi: Unpacking...")

        if msg.body.size_unzipped:
            with GzipFile(fileobj=BytesIO(msg.body.message_body)) as f:
                data = f.read()

            if len(data) != msg.body.size_unzipped:
                self._log.error('Unzipped size does not match! Disconnecting...')
                asyncio.ensure_future(self.disconnect())
                return
        else:
            data = msg.body.message_body

        while len(data) > 0:
            size, = struct.unpack_from("<I", data)
            await self.__parse_message(data[4:4 + size])
            data = data[4 + size:]

    async def __heartbeat(self, interval: int):
        message = MsgProto(EMsg.ClientHeartBeat)

        while self.connected:
            await asyncio.sleep(interval)
            if self.verbose_logging:
                self._log.debug("Sending heartbeat...")
            await self.send(message)

    async def _handle_logon(self, msg):
        result = msg.body.eresult

        if result != EResult.OK:
            asyncio.ensure_future(self.disconnect())
        else:
            self._log.debug("Logged on!")
            self.logged_on = True
            self.steam_id = SteamID(msg.header.steamid)
            self.session_id = msg.header.client_sessionid
            self.cell_id = msg.body.cell_id

            if self._heartbeat_task and not self._heartbeat_task.done():
                self._heartbeat_task.cancel()

            interval = msg.body.out_of_game_heartbeat_seconds
            self._heartbeat_task = asyncio.ensure_future(self.__heartbeat(interval))
            self._log.debug(f"Started hearbeat with interval: {interval}.")

            self._client.user = await self._client.get_user(self.steam_id)
            self.dispatcher('login')
            return

        if result in (EResult.TryAnotherCM, EResult.ServiceUnavailable):
            self.servers.mark_bad(self._current_server)
            self._log.debug('Disconnecting as the Server said we should')
        elif result == EResult.InvalidPassword:
            self._log.debug("Wrong password!")
            self.reconnect = False
            self.exception = InvalidPassword()
        elif result in (EResult.TwoFactorCodeMismatch, EResult.InvalidLoginAuthCode):
            self.reconnect = False
            self.exception = TwoFactorMismatch()
        elif result == EResult.AccountLoginDeniedNeedTwoFactor:
            self.reconnect = False
            self.exception = TwoFactorNeeded()
        else:
            self._log.error(f"Logon error: {result}")

    async def _handle_loginkey(self, msg):
        resp = MsgProto(EMsg.ClientNewLoginKeyAccepted)
        resp.body.unique_id = msg.body.unique_id

        if self.logged_on:
            self._log.debug('Accepting login_key...')
            await self.send(resp)
            self.login_key = msg.body.login_key

    async def _handle_licenses(self, msg):
        licenses = []
        for data in msg.body.licenses:
            licenses.append(License(data))
        self._client.licenses = licenses

    async def _handle_sentry(self, msg):
        self._log.debug("Handling sentry...")
        path = self._client.sentry_path
        try:
            ok = await store_sentry(path, msg.body.bytes)
        except Exception as e:
            self._log.warning("Failed to save sentry: ", exc_info=e)
        else:
            if ok:
                self._log.debug("Stored sentry! Sending response...")
                resp = MsgProto(EMsg.ClientUpdateMachineAuthResponse)

                resp.header.jobid_target = msg.header.jobid_source
                resp.body.filename = msg.body.filename
                resp.body.eresult = EResult.OK
                resp.body.sha_file = sha1_hash(msg.body.bytes)
                resp.body.getlasterror = 0
                resp.body.offset = msg.body.offset
                resp.body.cubwrote = msg.body.cubtowrite

                await self.send(resp)

    async def _handle_friendlist(self, msg):
        incremental = msg.body.bincremental

        new_users = []
        friends = {}
        _queue = []
        for friend in msg.body.friends:
            steamid = SteamID(friend.ulfriendid)

            if steamid.type != EUserType.Individual:
                continue

            user = await self._client.get_user(steamid, False)
            rel = EFriendRelationship(friend.efriendrelationship)

            if steamid not in self._client.friends and rel != EFriendRelationship.NONE:
                if rel == EFriendRelationship.Friend:
                    friends[steamid] = user
                user.relationship = rel
                new_users.append(steamid)

                if rel == EFriendRelationship.RequestRecipient:
                    _queue.append(('friend_invite', user))  # friend invite
            else:
                oldrel, user.relationship = user.relationship, rel

                if rel == EFriendRelationship.NONE:
                    self._client.friends.pop(steamid)
                    _queue.append(('friend_removed', user))  # friend removed
                elif oldrel in (EFriendRelationship.RequestRecipient,
                                EFriendRelationship.RequestInitiator) \
                        and rel == EFriendRelationship.Friend:
                    _queue.append(('friend_added', user))  # friend added

        if len(new_users) > 0:
            await self._client.fetch_state(new_users)

        if incremental:
            self._client.friends.update(friends)
        else:
            self._client.friends.set(friends)

        for event, user in _queue:
            self.dispatcher(event, user)

    async def _handle_persona_state(self, msg):
        for friend in msg.body.friends:
            steamid = friend.friendid

            if steamid in self._client._user_cache:
                user = self._client._user_cache[steamid]
                before = User(user._client, user.steamid)
                before._state = user._state
                before.parse()
                user._state = friend
                user.parse()

                if all([self._client.is_ready,
                        friend != before._state,
                        user.steamid != self._client.user.steamid,
                        user.ready]):
                    self.dispatcher('user_update', before, user)
                    if before.relationship in (
                            EFriendRelationship.RequestInitiator,
                            EFriendRelationship.RequestInitiator) and \
                            user.relationship == EFriendRelationship.Friend:
                        # Checks if the user now is our friend, as friendlist_update
                        # doesn't seem to do that
                        self.dispatcher('friend_added', user)

    async def _handle_new_chat_message(self, msg):
        if msg.body.chat_entry_type == EChatEntryType.ChatMsg:
            user = await self._client.get_user(msg.body.steamid_friend)
            try:
                await asyncio.wait_for(user._state_event.wait(), timeout=5)
            except asyncio.TimeoutError:
                self._log.debug('Timed out waiting for user data, '
                                'dispatching message even either way')
            self.dispatcher('message', user, msg.body.message)

    async def _handle_old_chat_message(self, msg):
        if msg.body.chat_entry_type == EChatEntryType.ChatMsg:
            user = await self._client.get_user(msg.body.steamid_from)
            try:
                await asyncio.wait_for(user._state_event.wait(), timeout=5)
            except asyncio.TimeoutError:
                self._log.debug('Timed out waiting for user data, '
                                'dispatching message even either way')
            self.dispatcher('message', user, msg.body.message.decode('utf-8'))

    async def _handle_limited_account(self, msg):
        self._client.is_limited_account = msg.body.bis_limited_account
        self._client.is_community_banned = msg.body.bis_community_banned
        self._client.is_locked_account = msg.body.bis_locked_account
        self._client.is_limited_account_allowed_to_invite_friends = \
            msg.body.bis_limited_account_allowed_to_invite_friends

    async def _handle_account_info(self, msg):
        self._client.account_name = msg.body.persona_name
        self._client.country = msg.body.ip_country

    async def _handle_email_info(self, msg):
        self._client.email = msg.body.email_address

    async def _handle_wallet_info(self, msg):
        bucks, cents = divmod(msg.body.balance64, 100)
        self._client.wallet_balance = (bucks, cents)

    async def _handle_vac_status(self, msg):
        self._client.vac_bans = msg.body.numBans

    async def _handle_clan_state(self, msg):
        self._client.clans.add(Clan(msg))


class Servers:
    """
    Manages the CM Servers

    Get server addresses by iterating over it
    Mark server as Bad with :meth:`mark_bad` when you cant connect to them
    """
    Good = 1
    Bad = 2
    _URL = "http://api.steampowered.com/ISteamDirectory/GetCMList/v1"

    def __init__(self):
        self._log = logging.getLogger(__name__)
        self._list = defaultdict(dict)
        self.cellid = None

    def __len__(self):
        return len(self._list)

    def __iter__(self):
        def iterator():
            if not self._list:
                self._log.error("No servers in list!")
                return

            good_servers = list(filter(lambda x: x[1]['quality'] == self.Good,
                                       self._list.items()
                                       ))

            if len(good_servers) == 0:
                self._log.debug("No good servers left. Resetting...")
                self.reset()
                return

            shuffle(good_servers)

            for server_addr, meta in good_servers:
                yield server_addr

        return iterator()

    def reset(self):
        """
        Resets all servers back to Good state
        """
        self._log.debug("Resetting all Servers to Good.")

        for key in self._list:
            self.mark_good(key)

    def mark_good(self, server_addr):
        """
        Marks server as Good
        :param server_addr: the server address that should be marked as good
        :type server_addr: :class:`tuple`
        """
        self._list[server_addr].update({'quality': self.Good, 'timestamp': time.time()})

    def mark_bad(self, server_addr):
        """
        Marks server as Bad
        :param server_addr: the server address that should be marked as bad
        :type server_addr: :class:`tuple`
        """
        self._log.debug(f"Marking {server_addr} as Bad.")
        self._list[server_addr].update({'quality': self.Bad, 'timestamp': time.time()})

    def clear(self):
        """
        Clears the internal server list
        """
        self._list.clear()

    def merge(self, new_servers):
        """
        Adds the given servers to the list of servers

        Parameters
        ----------
        new_servers: :class:`Iterable`
            A list of ip, port tuples to add to the list
        """
        total = len(self._list)
        for ip, port in new_servers:
            if (ip, port) not in self._list:
                self.mark_good((ip, port))

        self._log.debug(f"Added {len(self._list) - total} new CM servers")

    async def from_webapi(self, cellid=0):
        """
        Gets CM-Server adresses from the Steam web api

        :param cellid: cell id (0 = global)
        :type cellid: :class:`int`
        :return: success
        :rtype: :class:`bool`
        """
        self._log.debug('Gathering servers from web api')

        def str_to_tuple(a):
            i, p = a.split(':')
            return str(i), int(p)

        try:
            async with aiohttp.ClientSession() as session:
                async with session.get(self._URL, params={'cellid': cellid}) as resp:
                    data = await resp.json()
                    result = EResult(data['response']['result'])

                    if result != EResult.OK:
                        return False

                    self.cellid = cellid
                    data = data['response']['serverlist']
                    data = map(str_to_tuple, data)
                    for ip, port in data:
                        self._list[(ip, port)].update({
                            'quality': self.Good,
                            'timestamp': time.time()
                        })
        except Exception as e:
            self._log.error(f'Server gathering failed: {e}')
            return False
        return True
