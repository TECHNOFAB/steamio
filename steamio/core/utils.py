import fnmatch
import re
import socket
import struct
from importlib import import_module
from types import GeneratorType as _GeneratorType

from aiofile import AIOFile
from google.protobuf.message import Message as _ProtoMessageType

from .enums import EMsg
from .protobufs import steammessages_base_pb2, steammessages_clientserver_2_pb2, \
    steammessages_clientserver_login_pb2, \
    steammessages_clientserver_pb2, steammessages_clientserver_friends_pb2

cmsg_lookup_predefined = {
    EMsg.Multi: steammessages_base_pb2.CMsgMulti,
    EMsg.ClientToGC: steammessages_clientserver_2_pb2.CMsgGCClient,
    EMsg.ClientFromGC: steammessages_clientserver_2_pb2.CMsgGCClient,
    EMsg.ClientServiceMethod: steammessages_clientserver_2_pb2.CMsgClientServiceMethodLegacy,  # noqa: E501
    EMsg.ClientServiceMethodResponse: steammessages_clientserver_2_pb2.CMsgClientServiceMethodLegacyResponse,  # noqa: E501
    EMsg.ClientGetNumberOfCurrentPlayersDP: steammessages_clientserver_2_pb2.CMsgDPGetNumberOfCurrentPlayers,  # noqa: E501
    EMsg.ClientGetNumberOfCurrentPlayersDPResponse: steammessages_clientserver_2_pb2.CMsgDPGetNumberOfCurrentPlayersResponse,  # noqa: E501
    EMsg.ClientLogonGameServer: steammessages_clientserver_login_pb2.CMsgClientLogon,  # noqa: E501
    EMsg.ClientCurrentUIMode: steammessages_clientserver_2_pb2.CMsgClientUIMode,  # noqa: E501
    EMsg.ClientChatOfflineMessageNotification: steammessages_clientserver_2_pb2.CMsgClientOfflineMessageNotification,  # noqa: E501
}
cmsg_lookup = dict()

for proto_module in [
    steammessages_clientserver_pb2,
    steammessages_clientserver_2_pb2,
    steammessages_clientserver_friends_pb2,
    steammessages_clientserver_login_pb2,
]:
    cmsg_list = proto_module.__dict__
    cmsg_list = fnmatch.filter(cmsg_list, 'CMsg*')
    cmsg_lookup.update(dict(zip(map(lambda cmsg_name: cmsg_name.lower(), cmsg_list),
                                map(lambda cmsg_name: getattr(proto_module, cmsg_name),
                                    cmsg_list)
                                )))
service_lookup = {
    'Broadcast': '.protobufs.steammessages_broadcast_pb2',
    'BroadcastClient': '.protobufs.steammessages_broadcast_pb2',
    'Chat': '.protobufs.steammessages_chat_pb2',
    'ChatRoom': '.protobufs.steammessages_chat_pb2',
    'ChatRoomClient': '.protobufs.steammessages_chat_pb2',
    'ChatUsability': '.protobufs.steammessages_chat_pb2',
    'ChatUsabilityClient': '.protobufs.steammessages_chat_pb2',
    'ClanChatRooms': '.protobufs.steammessages_chat_pb2',
    'Cloud': '.protobufs.steammessages_cloud_pb2',
    'Credentials': '.protobufs.steammessages_credentials_pb2',
    'DataPublisher': '.protobufs.steammessages_datapublisher_pb2',
    'ValveHWSurvey': '.protobufs.steammessages_datapublisher_pb2',
    'ContentBuilder': '.protobufs.steammessages_depotbuilder_pb2',
    'DeviceAuth': '.protobufs.steammessages_deviceauth_pb2',
    'Econ': '.protobufs.steammessages_econ_pb2',
    'FriendMessages': '.protobufs.steammessages_friendmessages_pb2',
    'FriendMessagesClient': '.protobufs.steammessages_friendmessages_pb2',
    'GameNotifications': '.protobufs.steammessages_gamenotifications_pb2',
    'GameNotificationsClient': '.protobufs.steammessages_gamenotifications_pb2',
    'GameServers': '.protobufs.steammessages_gameservers_pb2',
    'Inventory': '.protobufs.steammessages_inventory_pb2',
    'InventoryClient': '.protobufs.steammessages_inventory_pb2',
    'CommunityLinkFilter': '.protobufs.steammessages_linkfilter_pb2',
    'Offline': '.protobufs.steammessages_offline_pb2',
    'Parental': '.protobufs.steammessages_parental_pb2',
    'PartnerApps': '.protobufs.steammessages_partnerapps_pb2',
    'PhysicalGoods': '.protobufs.steammessages_physicalgoods_pb2',
    'Player': '.protobufs.steammessages_player_pb2',
    'PlayerClient': '.protobufs.steammessages_player_pb2',
    'PublishedFile': '.protobufs.steammessages_publishedfile_pb2',
    'PublishedFileClient': '.protobufs.steammessages_publishedfile_pb2',
    'Secrets': '.protobufs.steammessages_secrets_pb2',
    'Shader': '.protobufs.steammessages_shader_pb2',
    'SiteLicense': '.protobufs.steammessages_site_license_pb2',
    'SiteManagerClient': '.protobufs.steammessages_site_license_pb2',
    'Store': '.protobufs.steammessages_store_pb2',
    'TwoFactor': '.protobufs.steammessages_twofactor_pb2',
    'AccountLinking': '.protobufs.steammessages_useraccount_pb2',
    'EmbeddedClient': '.protobufs.steammessages_useraccount_pb2',
    'UserAccount': '.protobufs.steammessages_useraccount_pb2',
    'FovasVideo': '.protobufs.steammessages_video_pb2',
    'Video': '.protobufs.steammessages_video_pb2',
    'VideoClient': '.protobufs.steammessages_video_pb2',
    'Clan': '.protobufs.steammessages_webui_friends_pb2',
    'Community': '.protobufs.steammessages_webui_friends_pb2',
    'ExperimentService': '.protobufs.steammessages_webui_friends_pb2',
    'FriendsList': '.protobufs.steammessages_webui_friends_pb2',
    'FriendsListClient': '.protobufs.steammessages_webui_friends_pb2',
    'SteamTV': '.protobufs.steammessages_webui_friends_pb2',
    'VoiceChat': '.protobufs.steammessages_webui_friends_pb2',
    'VoiceChatClient': '.protobufs.steammessages_webui_friends_pb2',
    'WebRTCClient': '.protobufs.steammessages_webui_friends_pb2',
    'WebRTCClientNotifications': '.protobufs.steammessages_webui_friends_pb2',
}

method_lookup = {}
MAGIC = b'VT01'
FMT = '<I4s'
FMT_SIZE = struct.calcsize(FMT)
protobuf_mask = 0x80000000
_list_types = list, range, _GeneratorType, map, filter


def proto_fill_from_dict(message, data, clear=True):
    if not isinstance(message, _ProtoMessageType):
        raise TypeError("Expected `message` to be a instance of protobuf message")
    if not isinstance(data, dict):
        raise TypeError("Expected `data` to be of type `dict`")

    if clear:
        message.Clear()
    field_descs = message.DESCRIPTOR.fields_by_name

    for key, val in data.items():
        desc = field_descs[key]

        if desc.type == desc.TYPE_MESSAGE:
            if desc.label == desc.LABEL_REPEATED:
                if not isinstance(val, _list_types):
                    raise TypeError((f"Expected {repr(key)} to be of type "
                                     f"list, got {type(val)}"))

                list_ref = getattr(message, key)

                # Takes care of overwriting list fields when merging partial data
                if not clear:
                    del list_ref[:]  # clears the list

                for item in val:
                    item_message = getattr(message, key).add()
                    proto_fill_from_dict(item_message, item)
            else:
                if not isinstance(val, dict):
                    raise TypeError((f"Expected {repr(key)} to be of type "
                                     f"list, got {type(dict)}"))

                proto_fill_from_dict(getattr(message, key), val)
        else:
            if isinstance(val, _list_types):
                list_ref = getattr(message, key)
                if not clear:
                    del list_ref[:]  # clears the list
                list_ref.extend(val)
            else:
                setattr(message, key, val)

    return message


def ip_from_int(i):
    return socket.inet_ntoa(struct.pack(">L", i))


def ip_to_int(ip):
    return struct.unpack(">L", socket.inet_aton(ip))[0]


def is_proto(emsg):
    return (int(emsg) & protobuf_mask) > 0


def clear_proto_bit(msg):
    return int(msg) & ~protobuf_mask


def set_proto_bit(emsg):
    return int(emsg) | protobuf_mask


def pad(s):
    return s + (16 - len(s) % 16) * struct.pack('B', 16 - len(s) % 16)


def unpad(s):
    return s[0:-s[-1]]


def get_cmsg(emsg):
    if not isinstance(emsg, EMsg):
        emsg = EMsg(emsg)

    if emsg in cmsg_lookup_predefined:
        return cmsg_lookup_predefined[emsg]
    else:
        enum_name = emsg.name.lower()
        if enum_name.startswith("econ"):  # special case for 'EconTrading_'
            enum_name = enum_name[4:]
        cmsg_name = "cmsg" + enum_name

    return cmsg_lookup.get(cmsg_name, None)


def get_um(method_name, response=False):
    key = (method_name, response)

    if key not in method_lookup:
        match = re.findall(r'^([a-z]+)\.([a-z]+)#(\d)?$', method_name, re.I)
        if not match:
            return None

        interface, method, version = match[0]

        if interface not in service_lookup:
            return None

        package = import_module(service_lookup[interface], package="steamio.core")

        service = getattr(package, interface, None)
        if service is None:
            return None

        for method_desc in service.GetDescriptor().methods:
            name = "%s.%s#%d" % (interface, method_desc.name, 1)

            method_lookup[(name, False)] = getattr(package,
                                                   method_desc.input_type.full_name, None)
            method_lookup[(name, True)] = getattr(package,
                                                  method_desc.output_type.full_name, None)

    return method_lookup[key]


async def store_sentry(path, data):
    async with AIOFile(path, 'wb') as file:
        await file.write(data)
        return True


async def get_sentry(path):
    try:
        async with AIOFile(path, 'rb') as file:
            return await file.read()
    except FileNotFoundError:
        return False
