# -*- coding: utf-8 -*-
import re

from .enums import EUserTypeChar, EUserType, EUniverse, EInstanceFlag

ETypeChars = ''.join(EUserTypeChar.__members__.keys())


class SteamID(int):
    def __new__(cls, *args, **kwargs):
        steam64 = make_steam64(*args, **kwargs)
        return super(SteamID, cls).__new__(cls, steam64)

    def __str__(self):
        return str(int(self))

    @property
    def id(self):
        return int(self) & 0xFFffFFff

    @property
    def instance(self):
        return (int(self) >> 32) & 0xFFffF

    @property
    def type(self):
        return EUserType((int(self) >> 52) & 0xF)

    @property
    def universe(self):
        return EUniverse((int(self) >> 56) & 0xFF)

    @property
    def as_32(self):
        return self.id

    @property
    def as_64(self):
        return int(self)

    @property
    def as_steam2(self):
        return "STEAM_%d:%d:%d" % (
            int(self.universe),
            self.id % 2,
            self.id >> 1,
        )

    @property
    def as_steam2_zero(self):
        return self.as_steam2.replace("_1", "_0")

    @property
    def as_steam3(self):
        typechar = str(EUserTypeChar(self.type))
        instance = None

        if self.type in (EUserType.AnonGameServer, EUserType.Multiseat):
            instance = self.instance
        elif self.type == EUserType.Individual:
            if self.instance != 1:
                instance = self.instance
        elif self.type == EUserType.Chat:
            if self.instance & EInstanceFlag.Clan:
                typechar = 'c'
            elif self.instance & EInstanceFlag.Lobby:
                typechar = 'L'
            else:
                typechar = 'T'

        parts = [typechar, int(self.universe), self.id]

        if instance is not None:
            parts.append(instance)

        return '[%s]' % (':'.join(map(str, parts)))

    @property
    def community_url(self):
        suffix = {
            EUserType.Individual: "profiles/%s",
            EUserType.Clan: "gid/%s",
        }
        if self.type in suffix:
            url = "https://steamcommunity.com/%s" % suffix[self.type]
            return url % self.as_64

        return None

    def is_valid(self):
        if self.type == EUserType.Invalid or self.type >= EUserType.Max:
            return False

        if self.universe == EUniverse.Invalid or self.universe >= EUniverse.Max:
            return False

        if self.type == EUserType.Individual:
            if self.id == 0 or self.instance > 4:
                return False

        if self.type == EUserType.Clan:
            if self.id == 0 or self.instance != 0:
                return False

        if self.type == EUserType.GameServer:
            if self.id == 0:
                return False

        if self.type == EUserType.AnonGameServer:
            if self.id == 0 and self.instance == 0:
                return False

        return True


def make_steam64(id=0, *args, **kwargs):
    accountid = id
    etype = EUserType.Invalid
    universe = EUniverse.Invalid
    instance = None

    if len(args) == 0 and len(kwargs) == 0:
        value = str(accountid)

        # numeric input
        if value.isdigit():
            value = int(value)

            # 32 bit account id
            if 0 < value < 2 ** 32:
                accountid = value
                etype = EUserType.Individual
                universe = EUniverse.Public
            # 64 bit
            elif value < 2 ** 64:
                return value
            # invalid account id
            else:
                accountid = 0

        # textual input e.g. [g:1:4]
        else:
            result = steam2_to_tuple(value) or steam3_to_tuple(value)

            if result:
                (accountid,
                 etype,
                 universe,
                 instance,
                 ) = result
            else:
                accountid = 0

    elif len(args) > 0:
        length = len(args)
        if length == 1:
            etype, = args
        elif length == 2:
            etype, universe = args
        elif length == 3:
            etype, universe, instance = args
        else:
            raise TypeError("Takes at most 4 arguments (%d given)" % length)

    if len(kwargs) > 0:
        etype = kwargs.get('type', etype)
        universe = kwargs.get('universe', universe)
        instance = kwargs.get('instance', instance)

    etype = (EUserType(etype)
             if isinstance(etype, (int, EUserType))
             else EUserType[etype]
             )

    universe = (EUniverse(universe)
                if isinstance(universe, (int, EUniverse))
                else EUniverse[universe]
                )

    if instance is None:
        instance = 1 if etype in (EUserType.Individual, EUserType.GameServer) else 0

    assert instance <= 0xffffF, "instance larger than 20bits"

    return (universe << 56) | (etype << 52) | (instance << 32) | accountid


def steam2_to_tuple(value):
    match = re.match(r"^STEAM_(?P<universe>\d+)"
                     r":(?P<reminder>[0-1])"
                     r":(?P<id>\d+)$", value
                     )

    if not match:
        return None

    steam32 = (int(match.group('id')) << 1) | int(match.group('reminder'))
    universe = int(match.group('universe'))

    # Games before orange box used to incorrectly display universe as 0, we support that
    if universe == 0:
        universe = 1

    return steam32, EUserType(1), EUniverse(universe), 1


def steam3_to_tuple(value):
    match = re.match(r"^\["
                     r"(?P<type>[i%s]):"  # type char
                     r"(?P<universe>[0-4]):"  # universe
                     r"(?P<id>\d{1,10})"  # accountid
                     r"(:(?P<instance>\d+))?"  # instance
                     r"\]$" % ETypeChars,
                     value
                     )
    if not match:
        return None

    steam32 = int(match.group('id'))
    universe = EUniverse(int(match.group('universe')))
    typechar = match.group('type').replace('i', 'I')
    etype = EUserType(EUserTypeChar[typechar])
    instance = match.group('instance')

    if typechar in 'gT':
        instance = 0
    elif instance is not None:
        instance = int(instance)
    elif typechar == 'L':
        instance = EInstanceFlag.Lobby
    elif typechar == 'c':
        instance = EInstanceFlag.Clan
    elif etype in (EUserType.Individual, EUserType.GameServer):
        instance = 1
    else:
        instance = 0

    instance = int(instance)

    return steam32, etype, universe, instance
