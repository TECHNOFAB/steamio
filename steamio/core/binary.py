from struct import unpack_from, calcsize


class StructReader(object):
    def __init__(self, data):
        if not isinstance(data, bytes):
            raise ValueError("Only works with bytes")
        self.data = data
        self.offset = 0

    def __len__(self):
        return len(self.data)

    def rlen(self):
        return max(0, len(self) - self.offset)

    def read(self, n=1):
        self.offset += n
        return self.data[self.offset - n:self.offset]

    def read_cstring(self, terminator=b'\x00'):
        null_index = self.data.find(terminator, self.offset)
        if null_index == -1:
            raise RuntimeError("Reached end of buffer")
        result = self.data[self.offset:null_index]  # bytes without the terminator
        self.offset = null_index + len(terminator)  # advance offset past terminator
        return result

    def unpack(self, format_text):
        data = unpack_from(format_text, self.data, self.offset)
        self.offset += calcsize(format_text)
        return data

    def skip(self, n):
        self.offset += n
