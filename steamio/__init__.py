# -*- coding: utf-8 -*-
from collections import namedtuple

__title__ = 'steamio'
__author__ = 'Technofab'
__license__ = 'MIT'
__copyright__ = 'Copyright 2020 Technofab'
__version__ = '0.0.2a'
__all__ = ['Client', 'InvalidPassword', 'TwoFactorMismatch', 'TwoFactorNeeded', 'License',
           'User', 'PersonaState', 'UserGame']

from .client import Client
from .errors import InvalidPassword, TwoFactorMismatch, TwoFactorNeeded
from .license import License
from .user import User, PersonaState
from .game import UserGame

VersionInfo = namedtuple('VersionInfo', 'major minor micro releaselevel serial')
version_info = VersionInfo(major=0, minor=0, micro=2, releaselevel='alpha', serial=0)
