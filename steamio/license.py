# -*- coding: utf-8 -*-
from .core.enums import EAppType
from .core.steamid import SteamID


class License:
    """
    Holds information about a game/software/movie etc. license

    Attributes
    ----------
    package_id: :class:`int`
        The steam package id
    playtime: :class:`int`
        The playtime in minutes
    playtime_limit: :class:`int`
        ?
    country_code: :class:`str`
        The code of the country where the license was redeemed
    type: :class:`steamio.EAppType`
        License type
    time_redeemed: :class:`int`
        Unix timestamp of when the license was redeemed
    payment_method: :class:`int`
        ?
    """
    def __init__(self, data):
        self.package_id = data.package_id
        self.playtime = data.minutes_used
        self.playtime_limit = data.minute_limit
        self.country_code = data.purchase_country_code
        self.type = EAppType(data.license_type)
        self.time_redeemed = data.time_created
        self.payment_method = data.payment_method
        self.access_token = data.access_token
        self.owner = SteamID(data.owner_id)
