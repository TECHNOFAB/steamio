# -*- coding: utf-8 -*-
import asyncio
import logging
import os
import signal
import sys
import time
import traceback
from enum import IntEnum

from .clan import ClanList
from .core.connection import Connection
from .core.enums import EMsg
from .core.msg import MsgProto
from .user import User, FriendList

log = logging.getLogger(__name__)


def _cancel_tasks(loop):
    try:
        task_retriever = asyncio.Task.all_tasks
    except AttributeError:
        task_retriever = asyncio.all_tasks

    tasks = {t for t in task_retriever(loop=loop) if not t.done()}

    if not tasks:
        return

    log.info(f'Cleaning up after {len(tasks)} tasks.')
    for task in tasks:
        task.cancel()

    loop.run_until_complete(asyncio.gather(*tasks, return_exceptions=True))
    log.info('All tasks finished cancelling.')

    for task in tasks:
        if task.cancelled():
            continue
        if task.exception() is not None:
            loop.call_exception_handler({
                'message': 'Unhandled exception during Client.run shutdown.',
                'exception': task.exception(),
                'task': task
            })


def _cleanup_loop(loop):
    try:
        _cancel_tasks(loop)
        if sys.version_info >= (3, 6):
            loop.run_until_complete(loop.shutdown_asyncgens())
    finally:
        log.info('Closing the event loop.')
        loop.close()


class Client:
    r"""
    Class to connect to the Steam API

    Parameters
    ----------
    username: Optional[:class:`str`]
        The username of the Steam Account. If None it will login as anonymous
    password: Optional[:class:`str`]
        The password of the Steam Account
    enable_sentry: Optional[:class:`bool`]
        Determines if the credentials/sentry should be saved in a file to not
        having to re-enter the email two factor codes. Defaults to `True`

        .. note::
           This wont work with Two Factor / Steam Guard enabled! Only Email Codes can be
           'bypassed' with sentries
    loop: Optional[:class:`asyncio.AbstractEventLoop`]
        Pass a custom loop here which the bot will use. Will default to
        asyncio's default loop
    verbose_logging: Optional[:class:`bool`]
        If enabled it will log a lot more information (with debug level)

    Attributes
    ----------
    username:
        The current username
    password:
        The current password
    is_closed: :class:`bool`
        `True` if the client is closed/not running
    sentry_path: :class:`str`
        The path where the sentry/credentials should be saved. The setter automatically
        generates a absolute path to the file from the given path
    licenses: :class:`list`
        List of licenses :class:`steamio.License` of the logged in user
    friends: :class:`FriendList`
        FriendList, behaving like a list, so you can iterate over it
    clans: :class:`ClanList`
        ClanList, behaving like a list, so you can iterate over it
    current_ui_mode: :class:`UIMode`
        The current UI mode, default is Desktop
    is_limited_account: :class:`bool`
        If the logged in account is a limited account
    is_community_banned: :class:`bool`
        If the logged in account is community banned
    is_locked_account: :class:`bool`
        If the logged in account is locked
    is_limited_account_allowed_to_invite_friends: :class:`bool`
        If the logged in account can invite friends (even though its limited?)
    """
    is_ready = False
    licenses = []
    user = None
    current_ui_mode = 0
    is_limited_account = None
    is_community_banned = None
    is_locked_account = None
    is_limited_account_allowed_to_invite_friends = None
    account_name = None
    country = None
    email = None
    wallet_balance = None
    vac_bans = None
    clans = ClanList()
    friends = FriendList()
    _event_queue = []
    _user_cache = {}
    _closed = False
    _sentry_path = '.'

    def __init__(self, username: str = None, password: str = None, login_key: str = None,
                 enable_sentry=False, loop=None, **kwargs):
        self._log = logging.getLogger(__name__)
        self._connection = Connection(self.dispatch, self,
                                      verbose_logging=kwargs.get('verbose_logging', False)
                                      )
        self.loop = loop if loop else asyncio.get_event_loop()
        self.enable_sentry = enable_sentry
        self.username = username
        self.password = password if login_key is None else login_key

    @property
    def username(self):
        return self._connection.username

    @username.setter
    def username(self, value):
        self._connection.username = value

    @property
    def password(self):
        return self._connection.username

    @password.setter
    def password(self, value):
        self._connection.password = value

    @property
    def reconnect(self):
        return self._connection.reconnect

    @reconnect.setter
    def reconnect(self, value: bool):
        self._connection.reconnect = value

    @property
    def is_closed(self):
        return self._closed

    @property
    def sentry_path(self):
        return os.path.join(self._sentry_path, f"{self.username}_sentry.bin")

    @sentry_path.setter
    def sentry_path(self, value):
        self._sentry_path = value

    @property
    def logged_on(self):
        return self._connection.logged_on

    @property
    def chat_mode(self):
        return self._connection.chat_mode

    @property
    def send(self):
        return self._connection.send

    @property
    def send_um(self):
        return self._connection.send_um

    async def start(self, reconnect=True, reconnect_delay: int = 1,
                    two_factor: str = None, auth_code: str = None):
        r"""
        Starts the bot and keeps it running if reconnect param is set to true (default)

        Parameters
        ----------
        reconnect: Optional[:class:`bool`]
            If the client should automatically reconnect if something goes wrong
            The client won't auto reconnect if something consistently stops the client
            (for example wrong credentials, as they're static and would cause a restart
            loop)
        reconnect_delay: Optional[:class:`int`]
            Seconds to wait before reconnecting
        two_factor: Optional[:class:`str`]
            The Two Factor Code (Steam Guard) used to login
        auth_code: Optional[:class:`str`]
            The Auth Code (Email) used to login
        """
        self.reconnect = reconnect
        self._log.debug("Starting")
        while not self.is_closed:
            self._connection.exception = None
            self._connection.two_factor_code = two_factor.upper() if two_factor else None
            self._connection.auth_code = auth_code.upper() if auth_code else None

            await self._connection.connect()
            await self._connection.disconnect_event.wait()

            if self._connection.exception:
                raise self._connection.exception

            self.dispatch("disconnect")
            if not self.reconnect and not self._closed:
                await self.close()

            if self.is_closed:
                return

            self._log.error(f'Attempting reconnect in {reconnect_delay}s')
            await asyncio.sleep(reconnect_delay)

    async def close(self):
        r"""
        Disconnects from the server and closes client.
        Does nothing if the client isn't running
        """
        if self._closed:
            return

        await self.logout()

        self._closed = True
        await self._connection.disconnect()

    async def logout(self):
        r"""
        Logs the current user out gracefully
        Does nothing if not connected or not logged in
        """
        if self._closed or not self.logged_on:
            return

        self._closed = True

        self._log.debug("Sending LogOff message")
        await self._connection.send(MsgProto(EMsg.ClientLogOff))

        try:
            await asyncio.wait_for(self._connection.disconnect_event.wait(), 5)
        except asyncio.TimeoutError:
            self._log.debug("Server didnt close the connection itself, disconnecting...")
            await self._connection.disconnect()

    async def get_user(self, steamid, fetch_state=True):
        r"""
        Fetches a user from its Steam ID

        Parameters
        ----------
        steamid: Union[:class:`int`, :class:`.SteamID`]
            The Steam ID of the user
        fetch_state: :class:`bool`
            If the user state should be fetched. Should normally always be True, only if
            you have a batch of users this should be False and you should fetch the states
            yourself for all users at once (for better performance)
        """
        steamid = int(steamid)
        user = self._user_cache.get(steamid)
        if user is None:
            user = User(self, steamid)
            self._user_cache[steamid] = user

            if fetch_state:
                await self.fetch_state([steamid])
        return user

    async def fetch_state(self, steamids, state=863):
        msg = MsgProto(EMsg.ClientRequestFriendData)
        msg.body.persona_state_requested = state
        msg.body.friends.extend(steamids)
        await self._connection.send(msg)

    async def change_status(self, status):
        r"""
        Changes the status of the logged in user

        Parameters
        ----------
        status: :class:`Status`
            The status to change to, like Online
        """
        msg = MsgProto(EMsg.ClientChangeStatus)
        await self._connection.send(msg, {
            'persona_state': status
        })

    async def change_ui_mode(self, mode):
        r"""
        Changes the UI mode of the logged in user

        Parameters
        ----------
        mode: :class:`UIMode`
            The ui mode to change to, like Desktop
        """
        self.current_ui_mode = mode
        msg = MsgProto(EMsg.ClientCurrentUIMode)
        await self.send(msg, {
            'uimode': UIMode(mode)
        })

    async def _run_event(self, coro, event_name, *args, **kwargs):
        try:
            await coro(*args, **kwargs)
        except asyncio.CancelledError:
            pass
        except Exception as e:
            try:
                await self.on_error(event_name, e, *args, **kwargs)
            except asyncio.CancelledError:
                pass

    def _update_ready_state(self):
        if self.is_ready:
            return

        if not self.clans.ready and self.clans.last_updated is not None:
            # allow new clans to be added within 1 second
            self.clans.ready = (time.time() - self.clans.last_updated) > 1

        if not self.friends.ready and self.friends.last_updated is not None:
            # allow new friends to be added within 1 second
            self.friends.ready = (time.time() - self.friends.last_updated) > 1

        friendlist = self.friends.ready
        clanlist = self.clans.ready
        userinfo = all([user.ready for user in self._user_cache.values()])

        if all([friendlist, clanlist, userinfo]):
            self.is_ready = True
            self.dispatch('ready')
            for [event, args, kwargs] in self._event_queue:
                self.dispatch(event, *args, **kwargs)

    def dispatch(self, event, *args, **kwargs):
        if not self.is_ready and event != 'login':
            self._log.debug(f"Enqueuing event {event} as the client is not ready yet")
            self._event_queue.append([event, args, kwargs])
            return
        self._log.debug(f"Dispatching event {event}")
        method = 'on_' + event

        try:
            coro = getattr(self, method)
        except AttributeError:
            pass
        else:
            asyncio.ensure_future(self._run_event(coro, method, *args, **kwargs))

    def event(self, function):
        r"""
        Registers an event with the functions name, for example: "on_ready"

        Example
        -------
        .. code-block:: python3

           @client.event
           async def on_ready():
               print("Client is ready!")

        Raises
        ------
        TypeError:
            Passed function is not a coroutine
        """
        if not asyncio.iscoroutinefunction(function):
            raise TypeError('Callback for an event has to be a coroutine!')

        setattr(self, function.__name__, function)
        self._log.debug(f"Registered event {function.__name__}")
        return function

    def run(self, *args, **kwargs):
        r"""
        Handles everything for you to start and stop the bot.
        This is blocking, everything that comes after this will not be
        called until this exits.
        All parameters will be passed to :meth:`Client.start`
        """
        self._log.info(("Running client... This is blocking, "
                        "nothing after this will be called until this returns"))
        try:
            self.loop.add_signal_handler(signal.SIGINT, lambda: self.loop.stop())
            self.loop.add_signal_handler(signal.SIGTERM, lambda: self.loop.stop())
        except NotImplementedError:
            pass

        async def runner():
            try:
                await self.start(*args, **kwargs)
            finally:
                await self.close()

        def stop_loop(f):
            self.loop.stop()

        future = asyncio.ensure_future(runner(), loop=self.loop)
        future.add_done_callback(stop_loop)
        try:
            self.loop.run_forever()
        except KeyboardInterrupt:
            self._log.info('Received signal, stopping client and event loop...')
        finally:
            future.remove_done_callback(stop_loop)
            _cleanup_loop(self.loop)

        if not future.cancelled():
            return future.result()

    async def on_error(self, event, *args, **kwargs):
        """
        Default error handler, overwrite to handle errors yourself
        """
        self._log.error(f"Error in {event}")
        traceback.print_exc()

    async def on_ready(self):
        """
        Default ready handler
        """
        pass

    async def on_login(self):
        """
        Default login handler. Sets the status of the logged in user to Online
        """
        await self.change_status(Status.Online)


class UIMode(IntEnum):
    Desktop = 0
    BigPicture = 1
    Mobile = 2
    Web = 3


class Status(IntEnum):
    Offline = 0
    Online = 1
    Busy = 2
    Away = 3
    Snooze = 4
    LookingToTrade = 5
    LookingToPlay = 6
    Invisible = 7
    Max = 8
