import time
from binascii import hexlify
from functools import reduce


class Clan:
    def __init__(self, msg):
        self._data = msg.body

    def _get(self, attr):
        try:
            return reduce(getattr, attr.split('.'), self._data)
        except AttributeError:
            return None

    @property
    def id(self):
        return self._get('steamid_clan')

    @property
    def name(self):
        return self._get('name_info.name')

    @property
    def members(self):
        return self._get('user_counts.members')

    @property
    def online(self):
        return self._get('user_counts.online')

    @property
    def chatting(self):
        return self._get('user_counts.chatting')

    @property
    def in_game(self):
        return self._get('user_counts.in_game')

    @property
    def chat_room_members(self):
        return self._get('user_counts.chat_room_members')

    def avatar_url(self, size=2):
        hashbytes = self._get('name_info.sha_avatar')

        if hashbytes != "\000\000\000\000\000\000\000\000\000\000\000" \
                        "\000\000\000\000\000\000\000\000\000":
            ahash = hexlify(hashbytes).decode('ascii')
        else:
            ahash = 'fef49e7fa7e1997310d705b2a6158ff8dc1cdfeb'

        sizes = {
            0: '',
            1: '_medium',
            2: '_full',
        }
        url = f"http://cdn.akamai.steamstatic.com/steamcommunity/public/images/" \
              f"avatars/{ahash[:2]}/{ahash}{sizes[size]}.jpg"

        return url


class ClanList:
    ready = False
    _clans = {}
    _last_update = None

    @property
    def last_updated(self):
        return self._last_update if self._last_update is not None else time.time()

    def set(self, clans):
        self._last_update = time.time()
        self._clans = clans

    def update(self, clans):
        self._last_update = time.time()
        self._clans.update(clans)

    def add(self, clan):
        self._last_update = time.time()
        self._clans[clan.id] = clan

    def __len__(self):
        return len(self._clans)

    def __iter__(self):
        return iter(self._clans.values())

    def __list__(self):
        return list(iter(self))

    def __getitem__(self, key):
        if isinstance(key, Clan):
            key = key.id
        return self._clans[key]

    def __contains__(self, clan):
        if isinstance(clan, Clan):
            clan = clan.id
        return clan in self._clans
