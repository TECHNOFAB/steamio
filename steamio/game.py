

class UserGame:
    def __init__(self, state):
        self._state = state

    def _get(self, attr):
        try:
            return getattr(self._state, attr)
        except AttributeError:
            return None

    def __repr__(self):
        name = "" if self.name == "" else f" NAME={self.name}"
        return f"<UserGame ID={self.id}{name}>"

    @property
    def id(self):
        return self._get('gameid') or self._get('game_played_app_id')

    @property
    def name(self):
        return self._get('game_name')

    @property
    def server_ip(self):
        return self._get('game_server_ip')

    @property
    def server_port(self):
        return self._get('game_server_port')

    @property
    def lobby_id(self):
        return self._get('game_lobby_id')
