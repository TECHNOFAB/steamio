# -*- coding: utf-8 -*-


class InvalidPassword(Exception):
    """
    Wrong password!
    """
    pass


class TwoFactorMismatch(Exception):
    """
    The given Two Factor code was wrong!
    """
    pass


class TwoFactorNeeded(Exception):
    """
    No Two Factor code was given, but one was needed!
    """
    pass
