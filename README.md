# SteamIO
[![No Maintenance Intended](http://unmaintained.tech/badge.svg)](http://unmaintained.tech/)
[![pipeline status](https://gitlab.com/TECHNOFAB/steamio/badges/master/pipeline.svg)](https://gitlab.com/TECHNOFAB/steamio/-/commits/master)
[![discord-chat](https://img.shields.io/discord/719924865393623102)](https://discord.gg/SuduTqw)

# This project is not maintained anymore!

Async Chat/Bot wrapper for Steam Chat made in Python

Heavily inspired by `ValvePython/steam` and `Rapptz/discord.py`

Please create a [Ticket/Issue](https://gitlab.com/TECHNOFAB/steamio/-/issues) if you encounter any bugs.
Please dont create an Issue with a Feature Request, as this project is still in the alpha

*Disclaimer: this is my first Python package, advice is welcome!*
# Documentation

Work in Progress

# Installation
**Python 3.6 or higher is required**
If you need help, feel free to join the [Discord Server](https://discord.gg/SuduTqw)

### PyPi
```shell script
** Work in Progress **
```

### Latest automatic build (TestPyPi)
```shell script
pip install --index-url https://test.pypi.org/simple/ steamio
```

### Latest development version
```shell script
git clone https://gitlab.com/TECHNOFAB/steamio
cd steamio
pip install .
```

# Quick example
```python
import steamio

client = steamio.Client(username="SomeAwesomeUsername", password="VerySecretPassword!")

@client.event
async def on_ready():
    print(f"Logged in as {client.user.name}")

@client.event
async def on_message(user, message):
    if message.startswith('!hi'):
        await user.reply('Hello!')

client.run()
```
