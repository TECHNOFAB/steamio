.. currentmodule:: steamio
.. _intro:

Introduction
============

SteamIO aims to provide an asynchronous wrappper around the Steam API.
It focuses on the Steam Chat and the interaction with users, but it is planned
to include more functionality, like getting information about a game etc.

Prerequisites
-------------

You will need Python 3.6 or higher due to aiohttp being used (which needs 3.5.3 or higher) and the
use of `f-Strings <https://www.python.org/dev/peps/pep-0498/>`_ (3.6+)

.. _installing:

Installing
----------

PyPi
~~~~
.. code-block:: shell

    pip install -U steamio
    # or
    python -m pip install -U steamio

Development Version
~~~~~~~~~~~~~~~~~~~
.. code-block:: shell

    git clone https://gitlab.com/TECHNOFAB/steamio
    cd steamio
    pip install .


.. toctree::