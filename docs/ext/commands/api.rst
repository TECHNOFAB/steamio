.. currentmodule:: steamio

API Reference
=============

Here you can see how the Bot extension works

Bot
---
.. autoclass:: steamio.ext.commands.Bot
   :members:
   :inherited-members:
