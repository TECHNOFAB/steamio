.. SteamIO documentation master file, created by
   sphinx-quickstart on Sat Apr 25 10:21:03 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SteamIO's documentation!
===================================

.. warning::

   This package is new and under heavy development! It will have bugs, especially at the beginning.
   Please create an Issue on `Gitlab <https://gitlab.com/TECHNOFAB/steamio/-/issues>`_ if you have problems with it.

SteamIO is an async wrapper for the Steam API with focus on the Steam Chat.
It's inspired by `steam <https://github.com/ValvePython/steam/>`_ and `discord.py <https://github.com/Rapptz/discord.py/>`_
and is intended to be used to create Steam bots

.. toctree::
   :maxdepth: 2

   intro
   api

Extensions
-----------

.. toctree::
  :maxdepth: 3

  ext/commands/index.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
